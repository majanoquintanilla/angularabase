<?php
namespace App\Models;
use Illuminate\Database\Eloquent\Model;
class User_jwt extends Model
{
  // protected $connection = 'nombre_conexion_en_database.php';
  protected $table = 'USER_JWT';
  protected $primary_key = 'ID';
  // protected $softDelete = true; // Eliminar en cascada
  // public $incrementing = true; // False si es autoincrementable
  public $timestamps = true;
  // protected $dateFormat = 'd-m-Y H:i:s';
  const CREATED_AT = 'CREATED_AT';
  const UPDATED_AT = 'UPDATED_AT';
  const DELETED_AT = 'DELETED_AT';
  protected $fillable = [
    "ID",
    "JTI",
    "IP",
    "ID_USER",
    "DUI",
    "ONI",
    "USER_AGENT",
    "CREATED_AT",
    "EXP_TIME",
    "IS_VALID",
    "IS_API",
    "VERSION",
    "DELETED_AT",
    "UPDATED_AT",
    "CREATED_TIME",
  ];
  public function User()
  {
    return $this->belongsTo('App\User', 'id', 'ID_USER');
  }
  // public function scopeDui($query, $value)
  // {
  //   if (!empty($value)) {
  //     return $query->where('DUI', $value);
  //   }
  // }
  // public function scopeDatetime($query, $fecha1, $fecha2)
  // {
  //   if (!empty($fecha1) && !empty($fecha2)) {
  //     return $query->whereBetween(\Illuminate\Support\Facades\DB::raw("CAST(CREATED_TIME AS date)"), [$fecha1, $fecha2]);
  //   }
  // }
  public function toArray()
  {
    $array = parent::toArray();
    $newArray = array();
    foreach ($array as $name => $value) {
      $newArray[strtolower($name)] = $value;
    }
    return $newArray;
  }
}
