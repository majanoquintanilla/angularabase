<?php
namespace App\Http\Middleware;
use App\Providers\RouteServiceProvider;
use App\User;
use App\Models\Mtto\Enlace;
use Illuminate\Support\Collection;
use Tymon\JWTAuth;
use Closure;
use Exception;
use Tymon\JWTAuth\Http\Middleware\BaseMiddleware;
class JwtMiddleware extends BaseMiddleware
{
  /**
   * Handle an incoming request.
   *
   * @param  \Illuminate\Http\Request  $request
   * @param  \Closure  $next
   * @return mixed
   */
  public function handle($request, Closure $next)
  {
    if (isset($request->debug_secret) && $request->debug_secret == env('APP_KEY') . env('JWT_SECRET')) { config(['app.debug' => true]); }
    try {
      $user = \JWTAuth::parseToken()->authenticate();
      if ($user) {
        $token = \JWTAuth::getToken()->get();
        $payload = \JWTAuth::manager()->getJWTProvider()->decode($token);
        // $macAddr = exec('getmac');
        // return response()->json($macAddr); 
        $isValid = jwtIsValid($token, $request, $payload);
        // return response()->json($request->all());
        if ($isValid["status"] == 200) {
          $isValid["payload"]["exp_time"] = $isValid["payload"]["exp"] - $isValid["payload"]["iat"];
          $isValid["payload"]["token"] = $token;
          $isValid["payload"]["lifetime"] = $isValid["payload"]["exp"] - time();
          $request->merge(['jwt' => $isValid]);
          return $next($request);
        } else {
          return response()->json($isValid); 
        }
      } else {
        return response()->json(['status' => 401, 'message' => "Unauthenticated.", 'code' => 5, "errorMessage" => "Usuario no encontrado", "responseJSON" => ["message" => "Unauthenticated."]]);
      }
    } catch (Exception $e) {
      if ($e instanceof JWTAuth\Exceptions\TokenInvalidException) {
        return response()->json(['status' => 401, 'message' => "Unauthenticated.", 'code' => 4, "errorMessage" => $e->getMessage(), "responseJSON" => ["message" => "Unauthenticated."]]);
      } else if ($e instanceof JWTAuth\Exceptions\TokenExpiredException) {
        return response()->json(['status' => 401, 'message' => "Unauthenticated.", 'code' => 3, "errorMessage" => $e->getMessage(), "responseJSON" => ["message" => "Unauthenticated."]]);
      } else if ($e instanceof JWTAuth\Exceptions\JWTException) {
        return response()->json(['status' => 401, 'message' => "Unauthenticated.", 'code' => 2, "errorMessage" => $e->getMessage(), "responseJSON" => ["message" => "Unauthenticated."]]);
      } else {
        return response()->json(['status' => 401, 'message' => "Unauthenticated.", 'code' => 1, "errorMessage" => $e->getMessage(), "responseJSON" => ["message" => "Unauthenticated."]]);
      }
    }
  }
}
