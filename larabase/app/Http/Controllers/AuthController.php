<?php
namespace App\Http\Controllers;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
class AuthController extends Controller
{
    public function __construct()
    {}
    public function login(Request $request)
    {
        $credentials = request(['email', 'password']);
        $exp_time = isset($request["exp_time"]) ? $request["exp_time"] : 3600;
        $platform = isset($request["platform"]) ? $request["platform"] : "";
        $pin = isset($request["pin"]) ? $request["pin"] : "";
        $jwt_res = jwtLogin($credentials, $exp_time, $platform, $pin);
        return response()->json($jwt_res);
    }
    public function register(Request $request)
    {
        $validator = \Illuminate\Validation\Validator::make($request->all(), [
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6|confirmed',
        ]);
        if($validator->fails()){
                return response()->json($validator->errors()->toJson(),400);
        }
        $user = User::create([
            'name' => $request->get('name'),
            'email' => $request->get('email'),
            'password' => Hash::make($request->get('password')),
        ]);
        // $exp_time = isset($request["exp_time"]) ? $request["exp_time"] : 3600;
        // $token = jwtLoginByUserId($user->id, $exp_time, 1);
        return response()->json($user);
    }
    public function me(Request $request)
    {
        $res = jwtMe();
        return response()->json($res);
    }
    public function logout(Request $request)
    {
        $res = jwtLogout();
        return response()->json($res);
    }
    public function refresh(Request $request)
    {
        return "";
    }
}