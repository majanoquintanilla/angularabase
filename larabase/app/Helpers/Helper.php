<?php
use App\User;
use App\User_jwt;
if (!function_exists('jwtIsValid')) {
    function jwtIsValid($token, $request = [], $payload = [])
    {
        try {
            $user_agent = isset($request["user_agent_socket"]) ? $request["user_agent_socket"] : $_SERVER['HTTP_USER_AGENT'];
            $user_agent_ = isset($payload["u"]) && $payload["u"] != "" ? \Illuminate\Support\Facades\Crypt::decrypt($payload["u"]) : "";
            $ip = isset($request["ip_socket"]) ? $request["ip_socket"] : (isset($_SERVER['HTTP_X_FORWARDED_FOR']) ? $_SERVER['HTTP_X_FORWARDED_FOR'] : getIp());
            $ip_ = isset($payload["i"]) && $payload["i"] != "" ? \Illuminate\Support\Facades\Crypt::decrypt($payload["i"]) : "";
            $jwt = isset($payload["jti"]) && $payload["jti"] != "" ? \App\Models\User_jwt::where("JTI", $payload["jti"])->first() : "";
            if (isset($jwt) && isset($jwt->ID_PARENT)) {
                \App\Models\User_jwt::where('ID', $jwt->ID_PARENT)->update(['IS_VALID' => 0]);
                $invalidate = jwtInvalidate($token);
                return ['status' => 401, 'message' => "", 'code' => 13, "errorMessage" => "jwt 2 invalido(1)", "responseJSON" => ["message" => ""]];
            } else if ((string)$user_agent != (string)$user_agent_) {
                $invalidate = jwtInvalidate($token);
                return ['status' => 401, 'message' => "Unauthenticated.", 'code' => 7, 'i' => $invalidate, "errorMessage" => "dispositivo invalido (1)", "responseJSON" => ["message" => "Unauthenticated."]];
            } else if (isset($jwt) && (int)$jwt->IS_VALID == 0) {
                $invalidate = jwtInvalidate($token);
                return ['status' => 401, 'message' => "Unauthenticated.", 'code' => 9, 'i' => $invalidate, "errorMessage" => "token invalido (1)", "responseJSON" => ["message" => "Unauthenticated."]];
            } else if ((string)$ip != (string)$ip_) {
                // $invalidate = jwtInvalidate($token);
                $pin = isset($request["pin"]) ? $request["pin"] : "";
                $pin_ = isset($payload["p"]) && $payload["p"] != "" ? \Illuminate\Support\Facades\Crypt::decrypt($payload["p"]) : "";
                $exp_time = isset($request["exp_time"]) ? $request["exp_time"] : 3600;
                if($pin != "" && $pin_ != "" && (string)$pin == (string)$pin_){
                    session(['pin' => $pin]);
                    return ['status' => 401, 'message' => "Unauthenticated.", 'code' => 12, 'i' => "", "errorMessage" => "ip invalida (2)", "responseJSON" => ["message" => "Unauthenticated."], "refresh" => jwtLoginByUserId($payload["sub"], $exp_time, 1)];
                } else {
                    return ['status' => 401, 'message' => "Unauthenticated.", 'code' => 6, 'i' => "", "errorMessage" => "ip invalida (1)", "responseJSON" => ["message" => "Unauthenticated."]];
                }
            } else {
                return ['status' => 200, 'message' => "", 'code' => 10, "errorMessage" => "jwt valido (1)", "responseJSON" => ["message" => ""], "payload" => $payload];
            }
        } catch (Exception $e) {
            return ["status" => 401, "message" => $e, 'code' => 11, "errorMessage" => "error indefinido", "responseJSON" => ["message" => "Unauthenticated."]];
        }
    }
}
if (!function_exists('jwtSave')) {
    function jwtSave($token, $token2, $is_api = 1, $version)
    {
        // dd($token);
        // try {
            // $user = \JWTAuth::toUser();
            $payload = \JWTAuth::manager()->getJWTProvider()->decode($token);
            // $user = \App\User::where('DUI', $payload['dui'])->first();
            $user_jwt = new \App\Models\User_jwt;
            $user_jwt->JTI = $payload["jti"];
            $user_jwt->IP = $payload["i"];
            $user_jwt->DUI = $payload["dui"];
            // $user_jwt->ONI = $user["oni"];
            $user_jwt->USER_AGENT = $payload["u"];
            $user_jwt->EXP_TIME = $payload["exp"];
            $user_jwt->IS_VALID = 1;
            $user_jwt->IS_API = $is_api;
            $user_jwt->VERSION = $version;
            $user_jwt->CREATED_TIME = time();
            if ($user_jwt->save()) {
                // TOKEN 2
                $payload2 = \JWTAuth::manager()->getJWTProvider()->decode($token2);
                // $user = \App\User::where('DUI', $payload2['dui'])->first();
                $user_jwt2 = new \App\Models\User_jwt;
                $user_jwt2->JTI = $payload2["jti"];
                $user_jwt2->IP = $payload2["i"];
                $user_jwt2->DUI = $payload2["dui"];
                // $user_jwt2->ONI = $user["oni"];
                $user_jwt2->USER_AGENT = $payload2["u"];
                $user_jwt2->EXP_TIME = $payload2["exp"];
                $user_jwt2->IS_VALID = 1;
                $user_jwt2->IS_API = $is_api;
                $user_jwt2->VERSION = $version;
                $user_jwt2->ID_PARENT = $user_jwt->id;
                $user_jwt2->CREATED_TIME = time();
                $user_jwt2->save();
                return ["status" => 200, "data" => $user_jwt, "data2" => $user_jwt2, "message" => "", "jwt_id" => $user_jwt->id];
            }
        // } catch (Exception $e) {
        //     return ["status" => 400, "message" => $e];
        // }
    }
}
if (!function_exists('jwtLogin')) {
    function jwtLogin($credentials, $exp_time = 480, $version = "", $pin = "")
    {
        // $now = \Carbon\Carbon::now()->timestamp;
        // $exp = \Carbon\Carbon::now()->addSeconds($exp_time)->timestamp;
        // $sum = $exp - $now;
        // dd(["exp"=>$exp,"now"=>$now,"sum"=>$sum]);
        // try {
            // $myTTL = 30; //minutes
            \JWTAuth::factory()->setTTL($exp_time);
            session(['pin' => $pin]);
            $token = auth('api')->attempt($credentials);
            if ($token) {
                $token2 = auth('api')->attempt($credentials);
                $jwtSave = jwtSave($token, $token2, 1, $version);
                return ["status" => 200, "data" => $token, "data2" => $token2, "message" => "", "jwt_id" => $jwtSave["jwt_id"]];
            } else {
                return ["status" => 400, "data" => null, "message" => "Credenciales no validas"];
            }
        // } catch (Exception $e) {
        //     return ["status" => 400, "message" => $e];
        // }
    }
}
if (!function_exists('jwtPinWeb')) {
    function jwtPinWeb()
    {
        if (session('jwtPinWeb')) {
            return session('jwtPinWeb');
        } else {
            session(['jwtPinWeb' => \Illuminate\Support\Str::orderedUuid()]);
            return session('jwtPinWeb');
        }
    }
}
if (!function_exists('jwtLoginByUserId')) {
    function jwtLoginByUserId($user_id, $exp_time = 3600, $is_api = 0)
    {
        try {
            $user = User::where("ID", $user_id)->first();
            \JWTAuth::factory()->setTTL($exp_time);
            $token = auth('api')->login($user);
            if ($token) {
                $token2 = auth('api')->login($user);
                $jwtSave = jwtSave($token, $token2, $is_api, "");
                return ["status" => 200, "data" => $token, "data2" => $token2, "message" => "", "jwt_id" => $jwtSave["jwt_id"]];
            } else {
                return ["status" => 400, "data" => null, "message" => "No se pudo crear el token"];
            }
        } catch (Exception $e) {
            return ["status" => 400, "message" => $e];
        }
    }
}
if (!function_exists('jwtInvalidate')) {
    function jwtInvalidate($token)
    {
        try {
            // $payload = \JWTAuth::manager()->getJWTProvider()->decode($token);
            // \App\Models\User_jwt::where('JTI', $payload["jti"])->update(['IS_VALID' => 0]);
            // $data = auth('api')->invalidate(true);
            // \JWTAuth::manager()->invalidate(new \Tymon\JWTAuth\Token($token), $forceForever = false);
            return ["status" => 200, "data" => "", "message" => "token invalidado"];
        } catch (Exception $e) {
            return ["status" => 400, "data" => null, "message" => $e];
        }
    }
}
if (!function_exists('jwtLogout')) {
    function jwtLogout()
    {
        try {
            $data = auth('api')->logout(true);
            return ["status" => 200, "data" => $data, "message" => ""];
        } catch (Exception $e) {
            return ["status" => 400, "data" => null, "message" => $e];
        }
    }
}
if (!function_exists('jwtMe')) {
    function jwtMe()
    {
        try {
            $data = auth('api')->user();
            return ["status" => 200, "data" => $data, "message" => ""];
        } catch (Exception $e) {
            return ["status" => 400, "data" => null, "message" => $e];
        }
    }
}
if (!function_exists('jwtRefresh')) {
    function jwtRefresh()
    {
        try {
            $data = auth('api')->refresh(true);
            return ["status" => 200, "data" => $data, "message" => ""];
        } catch (Exception $e) {
            return ["status" => 400, "data" => null, "message" => $e];
        }
    }
}
if (!function_exists('jwtToken')) {
    function jwtToken($numero = 1)
    {
        if (auth()->user()) {
            if (session('jwt_token1')) {
                $jwt_token1 = session('jwt_token1');
                $isValid = jwtIsValid($jwt_token1,request(),\JWTAuth::manager()->getJWTProvider()->decode($jwt_token1));
                if ($isValid["status"] == 200) {
                    return ["jwt_token1" => $jwt_token1, "jwt_token2" => session('jwt_token2'), "isValid"=>$isValid];
                } else {
                    $userId = auth()->user()->id;
                    $jwt_res = jwtLoginByUserId($userId);
                    if ($jwt_res["status"] == 200) {
                        session(['jwt_token1' => $jwt_res["data"]]);
                        session(['jwt_token2' => $jwt_res["data2"]]);
                        return ["jwt_token1" => $jwt_res["data"], "jwt_token2" => $jwt_res["data2"]];
                    } else {
                        session(['jwt_token1' => ""]);
                        session(['jwt_token2' => ""]);
                        return ["jwt_token1" => "", "jwt_token2" => "", 'message' => "Credenciales Invalidas o usuario no existe",];
                    }
                }
            } else {
                $userId = auth()->user()->id;
                $jwt_res = jwtLoginByUserId($userId);
                if ($jwt_res["status"] == 200) {
                    session(['jwt_token1' => $jwt_res["data"]]);
                    session(['jwt_token2' => $jwt_res["data2"]]);
                    return ["jwt_token1" => $jwt_res["data"], "jwt_token2" => $jwt_res["data2"]];
                } else {
                    session(['jwt_token1' => ""]);
                    session(['jwt_token2' => ""]);
                    return ["jwt_token1" => "", "jwt_token2" => "", 'message' => "Credenciales Invalidas o usuario no existe",];
                }
            }
        } else {
            session(['jwt_token1' => ""]);
            session(['jwt_token2' => ""]);
            return ["jwt_token1" => "", "jwt_token2" => "", 'message' => "Ususario no logueado",];
        }
    }
}
if (!function_exists('getIp')) {
    function getIp()
    {
        foreach (array('HTTP_CLIENT_IP', 'HTTP_X_FORWARDED_FOR', 'HTTP_X_FORWARDED', 'HTTP_X_CLUSTER_CLIENT_IP', 'HTTP_FORWARDED_FOR', 'HTTP_FORWARDED', 'REMOTE_ADDR', 'X-Forwarded-For') as $key) {
            if (array_key_exists($key, $_SERVER) === true) {
                foreach (explode(',', $_SERVER[$key]) as $ip) {
                    $ip = trim($ip); // just to be safe
                    if (filter_var($ip, FILTER_VALIDATE_IP, FILTER_FLAG_NO_PRIV_RANGE | FILTER_FLAG_NO_RES_RANGE) !== false) {
                        return $ip;
                    }
                }
            }
        }
        return request()->ip(); // it will return server ip when no client ip found
    }
}