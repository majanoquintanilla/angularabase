import { Routes } from '@angular/router';
import { AuthGuard } from './helpers/auth.guard';
export const appRoutes: Routes = [
    { path: '', redirectTo: 'loader', pathMatch: 'full' },
    { path: 'loader', loadChildren: () => import('./pages/loader/loader.module').then(m => m.LoaderModule) },
    { path: 'login', loadChildren: () => import('./pages/login/login.module').then(m => m.LoginModule) },
    { path: 'home', loadChildren: () => import('./pages/home/home.module').then(m => m.HomeModule), canActivate: [AuthGuard], data: { preload: true } },
    { path: '', redirectTo: 'home', pathMatch: 'full' },
    { path: '**', redirectTo: 'loader', pathMatch: 'full' },
];