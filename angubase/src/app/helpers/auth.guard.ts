import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, RouterStateSnapshot, UrlTree } from '@angular/router';
import { Observable } from 'rxjs';
import { CoreService } from "./../services/core.service";

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {
  constructor(
    private core: CoreService,
  ) { }
  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
      return new Promise(resolve => {
        this.core.auth.getUserLocal('guard').then((res:any) => {
         // console.log(res);
         if (!!res && res["success"]) {
           resolve(true);
         } else {
           this.core.helper.goToPage('login');
           resolve(false);
          }
        }, (e:any) => {
         this.core.helper.goToPage('login');
         console.log(e);
        //  this.core.router.navigateRoot('login'); resolve(false);
       });
     });
  }
  
}
