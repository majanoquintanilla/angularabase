import { Injectable } from '@angular/core';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class HelperService {
  constructor(
    private router: Router,

  ) { }
  public core: any; public setCore(core: any) { this.core = core; return this; }
  public goToPage(page = "", param = {}, dir = "forward") {
    if (page != "") {
      // dir == "forward" ? this.nav.navigateForward([`/${page}`, param]) : this.nav.navigateBack([`/${page}`, param]);
      this.router.navigate([`/${page}`, param]);
    }
  }
}
