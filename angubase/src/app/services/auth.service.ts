import { Injectable } from '@angular/core';
import { User } from '../models/user.model';
@Injectable({
  providedIn: 'root'
})
export class AuthService {
  public user: User = new User;
  constructor() { }
  public core: any; public setCore(core: any) { this.core = core; return this; }
  public async getUserLocal(mensaje : any): Promise<any> {
    return new Promise((resolve) => {
      let user = localStorage.getItem('user') ? JSON.parse(localStorage.getItem('user') || '{}') : '{}';
      resolve(user);
    });
  };
  async setUserLocal({ user }: any) {
    localStorage.setItem('user', JSON.stringify(user));
  }
  async Login(email:any, password:any, exp_time?: any) {
    return new Promise((resolve) => {
        this.core.api.doRequest('login', { email, password, exp_time: exp_time }, 'post').then((res: any) => {
          console.log(res);
          if (res.hasOwnProperty("success") && res.success) {
            this.user = res;
            resolve(true);
          }
          else if (res.hasOwnProperty("success") && !res.success) {
            // this.util.showToast(res['message']);
            resolve(false);
          } else {
            // this.util.dismissLoading();
            // this.core.helper.alert("No fue posible conectar", "", "");
            resolve(false);
          }
        }).catch((e: any) => {
          // this.core.helper.showToast('No se inicio sesion');
          console.log(e);
          resolve(false);
        });
    });
  }
}
