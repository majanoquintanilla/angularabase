import { Injectable } from '@angular/core';
import { Subscription, throwError } from 'rxjs';
import { catchError, distinct } from 'rxjs/operators';
import { HttpClient, HttpHeaders, HttpParams, HttpErrorResponse } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ApiService {
  subs: Subscription = new Subscription;
  constructor(
    public http: HttpClient,

  ) { }
  public core: any; public setCore(core: any) { this.core = core; return this; }
  async doRequest_({ url_, params_, type_, headers_ }: any): Promise<any> {
    return await new Promise((resolve) => {
      resolve(this.doRequest(url_, params_, type_, headers_));
    });
  }

  api: any;
  async doRequest(url_: string, params_: any, type_: string, headers_ = ""): Promise<any> {
    this.api = `${this.core.server_url}/api/v1`;
    try {
      // console.log(url_)
      // console.log(params_)
      // console.log(type_)
      let url = url_.includes("http:") || url_.includes("https:") ? url_ : `${this.api}/${url_}`;
      // let url = `${this.api}/${url_}`;
      // let token = "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC8xOTIuMTY4LjI3LjI1OjkwOTFcL2FwaVwvdjFcL2xvZ2luIiwiaWF0IjoxNjYzOTQyNjc1LCJleHAiOjE2NjQwNTA2NzUsIm5iZiI6MTY2Mzk0MjY3NSwianRpIjoiRmZmQXlBWlpBSU1CcUhSdiIsInN1YiI6MzkxOTYsInBydiI6Ijg3ZTBhZjFlZjlmZDE1ODEyZmRlYzk3MTUzYTE0ZTBiMDQ3NTQ2YWEiLCJkdWkiOiIwNTI4NTM1MS00IiwiaXAiOiIxMC4wLjIuMiIsInVzZXJfYWdlbnQiOiJNb3ppbGxhXC81LjAgKExpbnV4OyBBbmRyb2lkIDExOyBQaXhlbCA1KSBBcHBsZVdlYktpdFwvNTM3LjM2IChLSFRNTCwgbGlrZSBHZWNrbykgQ2hyb21lXC85MC4wLjQ0MzAuOTEgTW9iaWxlIFNhZmFyaVwvNTM3LjM2IiwiZGF0ZSI6IjIwMjItMDktMjMgMDg6MTc6NTUiLCJrZXl3b3JkcyI6W119.Ewi5BDWuoEY_uzLWW1HYGePfgBY73nnflBwk_hYXoYo";
      let token = headers_;
      let headers = { headers: new HttpHeaders({ 'Content-Type': 'application/json', 'Authorization': `Bearer ` + token + `` }) };
      let params = params_;
      return await new Promise((resolve) => {
        if (type_ === 'post') {
          try {
            this.subs = this.http.post(url, params, headers).pipe(
              catchError(err => { return throwError(err); })
            ).subscribe(
              (res: any) => {
                res['name'] === 'HttpErrorResponse' ? resolve(this.handleResponse(res['error'])) : resolve(this.handleResponse(res));
              }, (err) => {
                resolve(this.handleResponse(err['error']))
              }
            );
          } catch (e) { console.log() }
        } else {
          this.subs = this.http.get(url, headers).subscribe(
            (res) => { resolve(this.handleResponse(res)); },
            (err) => { resolve(this.handleResponse(err['error'])) }
          );
        }
      }).catch((e) => { console.log(e) })
    } catch (e : any) {
      console.log(e)
      console.log(this.handleResponse(e.error))
    }
  }
  async CancelRequest() { !!this.subs ? this.subs.unsubscribe() : null; }
  async handleResponse(res: any): Promise<any> {
    return await new Promise((resolve, reject) => {
      if (res.type != undefined && res.type == 'error') {
        console.log(navigator.onLine)
        if (!navigator.onLine) {
          resolve({ status: false, "message": "No hay conexion a internet" });
        }
        console.log(res.type)
      }
      else {
        this.subs.closed
        // console.log(this.subs)
        if (res && res.hasOwnProperty('message') && res['message'] == "Unauthenticated.") {
          if (res['code'] == 2) {
            // console.log('Unauth');
            // this.core.util.showToast('Desautorizado, Vuelva a introducir su clave')
            // this.core.helper.dismissLoading();
            resolve(this.core.helper.goToPage('lockscreen', {}, "back"));
          }
          else if (res['code'] == 3) {
            console.log('Unauth');
            this.core.helper.showToast("La sesión ha expirado");
            this.core.event.publish('logout', true);
            resolve(this.core.helper.goToPage('login', {}, "back"));
          }
          else {
            console.log('Unauth');
            // this.pwa.util.showToast("La sesión ha expirado");
            this.core.event.publish('logout', true);
            resolve(this.core.helper.goToPage('login', {}, "back"));
          }
        } else {
          // console.log(res);
          resolve(res);
        }
      }
    });
  }
  private handleError(error: HttpErrorResponse) {
    if (error.error instanceof ErrorEvent) { console.error('An error occurred:', JSON.stringify(error)); }
    else { console.error('Backend returned code ' + error.status + ' body was:' + JSON.stringify(error)); }
    return throwError('Something bad happened; please try again later.');
  }
  async Login() {
    return this.doRequest(`${this.api}/login`, {}, 'post');
  }
  async me(token: any) {
    return this.doRequest("me", {}, 'post', token)
  }
}
