import { Injectable } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { ApiService } from "./api.service"
import { AuthService } from "./auth.service";
import { HelperService } from "./helper.service";

@Injectable({
  providedIn: 'root'
})
export class CoreService {
  public serverUrl = "http://localhost:84";
  private core_ = this;
  public auth: AuthService;
  public api: ApiService;
  public helper: HelperService;
  constructor(
    private ApiService: ApiService,
    private AuthService: AuthService,
    private HelperService: HelperService,
  ) {
    this.auth = AuthService.setCore(this);
    this.api = ApiService.setCore(this);
    this.helper = HelperService.setCore(this);
  }
}
