import { Component } from '@angular/core';
import { CoreService } from 'src/app/services/core.service';

@Component({
  selector: 'app-loader',
  templateUrl: './loader.component.html',
  styleUrls: ['./loader.component.scss']
})
export class LoaderComponent {
  constructor(
    public core:CoreService
  ){
    this.core.auth.getUserLocal('loader').then((res) => {
      console.log(res)
      if (res != null) {
        this.core.helper.goToPage("home")
      } else {
        this.core.helper.goToPage("login")
      }
    });
  }
}
