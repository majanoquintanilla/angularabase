* El proposito de este proyecto es una tener una estructura base entre laravel y angular con autenticacion de sesion web y api jwt token, extra seguridad en el manejo de token

* Ejecutar el script en un editor de mysql utilizando el archivo angularabase.sql

* Para ejecutar el backend entrar a directorio larabase y ejecutar : composer install && php artisan serve --port=84

* Para ejecutar el frontend entrar a directorio angubase y ejecutar : npm install && ng serve --port=85

* A continuacion se muestran los pasos a detalle de la creacion de ambos proyectos , primero laravel y luego angular :

1) Crear proyecto de laravel en la terminal :
```console
composer create-project laravel/laravel:^8.* larabase
```

2) Agregar la conexion en el archivo /.env :
```sql
DB_CONNECTION=mysql
DB_HOST=127.0.0.1
DB_PORT=3306
DB_DATABASE=base_angular_laravel
DB_USERNAME=root
DB_PASSWORD=
```

3) Instalar paquete para autenticacion :
```console
composer require laravel/ui
```

4) Generar views y rutas de autenticacion :
```console
php artisan ui bootstrap --auth
```

5) Crear tablas de autenticación en la base :
```console
php artisan migrate
```

6) Instalar paguetes de bootstrap que vienen por defecto en node y compilarlos :
```console
npm install && npm run dev
```

7) Inicial el proyecto en la web en cualquier puerto :
```console
php artisan serve --port=84 
```

8) Ir al navegador :
```console
localhost:84
```

9) Registrar un usuario en la url:
```console
localhost:84/register
```

10) Instalar paquete para generar tokens :
```console
composer require tymon/jwt-auth
```

11)Agregar "service provider" al archivo /config/app.php :
```php
'providers' => [
    /*  ...  */
    Tymon\JWTAuth\Providers\LaravelServiceProvider::class,
],
'aliases' => [
    /*  ...  */
    'JWTAuth' => Tymon\JWTAuth\Facades\JWTAuth::class,
    'JWTFactory' => Tymon\JWTAuth\Facades\JWTFactory::class,
],
```

12) Publicar archivo de configuracion de token :
```console
php artisan vendor:publish --provider="Tymon\JWTAuth\Providers\LaravelServiceProvider"
```

13) Generar frase secreta para firmar los tokens en el archivo /.env :
```console
php artisan jwt:secret
```

14) Agregar en /app/Models/User.php la siguente estructura y metodos:
```php
<?php
namespace App;
use Tymon\JWTAuth\Contracts\JWTSubject;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
class User extends Authenticatable implements JWTSubject
{
    use Notifiable;
    // Rest omitted for brevity
    /**
     * Get the identifier that will be stored in the subject claim of the JWT.
     *
     * @return mixed
     */
    public function getJWTIdentifier()
    {
        return $this->getKey();
    }
    /**
     * Return a key value array, containing any custom claims to be added to the JWT.
     *
     * @return array
     */
    public function getJWTCustomClaims()
    {
        return [
            // 'dui'         => $this->dui,
            'p'           => !!session('pin') ? \Illuminate\Support\Facades\Crypt::encrypt(session('pin')) : "",
            'i'           => isset($_SERVER['HTTP_X_FORWARDED_FOR']) ? \Illuminate\Support\Facades\Crypt::encrypt($_SERVER['HTTP_X_FORWARDED_FOR']) : \Illuminate\Support\Facades\Crypt::encrypt(getIp()),
            'u'           => \Illuminate\Support\Facades\Crypt::encrypt($_SERVER['HTTP_USER_AGENT']),
            'date'        => date("Y-m-d H:i:s"),
            'keywords'    => [],
        ];
    }
}
```

15) Modificar la siguente estructura en el archivo /config/auth.php para agregar el guard de "api" :
```php
    'guards' => [
        'web' => [
            'driver' => 'session',
            'provider' => 'users',
        ],
        'api' => [
            'driver' => 'jwt',
            'provider' => 'users',
            'hash' => false,
        ],
    ],
```

16) Agregar las siguente rutas en el archivo /routes/api.php :
```php
Route::group(['prefix' => 'v1'], function ($router) {
    Route::post('login', [\App\Http\Controllers\AuthController::class, 'login']);
    Route::post('register', [\App\Http\Controllers\AuthController::class, 'register']);
    Route::group(['middleware' => ['jwt']], function ($router) {
        Route::post('logout', [\App\Http\Controllers\AuthController::class, 'logout']);
        Route::post('refresh', [\App\Http\Controllers\AuthController::class, 'refresh']);
        Route::post('me', [\App\Http\Controllers\AuthController::class, 'me']);
        Route::get('me', [\App\Http\Controllers\AuthController::class, 'me']);
    });
});
```

17) Crear el controlador con el siguente comando :
```console
php artisan make:controller AuthController
```

18) Copiar y pegar el siguente codigo en el archivo /app/Http/Controllers/AuthController.php :
```php
<?php
namespace App\Http\Controllers;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
class AuthController extends Controller
{
    public function __construct()
    {}
    public function login(Request $request)
    {
        $credentials = request(['email', 'password']);
        $exp_time = isset($request["exp_time"]) ? $request["exp_time"] : 3600;
        $platform = isset($request["platform"]) ? $request["platform"] : "";
        $pin = isset($request["pin"]) ? $request["pin"] : "";
        $jwt_res = jwtLogin($credentials, $exp_time, $platform, $pin);
        return response()->json($jwt_res);
    }
    public function register(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6|confirmed',
        ]);
        if($validator->fails()){
                return response()->json($validator->errors()->toJson(),400);
        }
        $user = User::create([
            'name' => $request->get('name'),
            'email' => $request->get('email'),
            'password' => Hash::make($request->get('password')),
        ]);
        // $exp_time = isset($request["exp_time"]) ? $request["exp_time"] : 3600;
        // $token = jwtLoginByUserId($user->id, $exp_time, 1);
        return response()->json($user);
    }
    public function me(Request $request)
    {
        $res = jwtMe();
        return response()->json($res);
    }
    public function logout(Request $request)
    {
        $res = jwtLogout();
        return response()->json($res);
    }
    public function refresh(Request $request)
    {
        return "";
    }
}
```

19) Ejecutar en mysql el siguente script para generar tabla de token :
```sql
CREATE TABLE user_jwt (
	ID int primary key NOT NULL AUTO_INCREMENT,
	JTI varchar(4000) NULL,
	IP varchar(256) NULL,
	DUI varchar(100) NULL,
	ONI varchar(100) NULL,
	USER_AGENT varchar(1500) NULL,
	CREATED_AT datetime(6) NULL,
	EXP_TIME varchar(100) NULL,
	IS_VALID int NULL,
	IS_API int NULL,
    ID_PARENT int NULL,
	VERSION varchar(100) NULL,
	DELETED_AT datetime(6) NULL,
	UPDATED_AT datetime(6) NULL,
	CREATED_TIME varchar(100) NULL
)
```

20) Crear el archivo /app/Models/User_jwt.php y copiar el siguente codigo :
```php
<?php
namespace App\Models;
use Illuminate\Database\Eloquent\Model;
class User_jwt extends Model
{
  // protected $connection = 'nombre_conexion_en_database.php';
  protected $table = 'USER_JWT';
  protected $primary_key = 'ID';
  // protected $softDelete = true; // Eliminar en cascada
  // public $incrementing = true; // False si es autoincrementable
  public $timestamps = true;
  // protected $dateFormat = 'd-m-Y H:i:s';
  const CREATED_AT = 'CREATED_AT';
  const UPDATED_AT = 'UPDATED_AT';
  const DELETED_AT = 'DELETED_AT';
  protected $fillable = [
    "ID",
    "JTI",
    "IP",
    "ID_USER",
    "DUI",
    "ONI",
    "USER_AGENT",
    "CREATED_AT",
    "EXP_TIME",
    "IS_VALID",
    "IS_API",
    "VERSION",
    "DELETED_AT",
    "UPDATED_AT",
    "CREATED_TIME",
  ];
  public function User()
  {
    return $this->belongsTo('App\User', 'id', 'ID_USER');
  }
  // public function scopeDui($query, $value)
  // {
  //   if (!empty($value)) {
  //     return $query->where('DUI', $value);
  //   }
  // }
  // public function scopeDatetime($query, $fecha1, $fecha2)
  // {
  //   if (!empty($fecha1) && !empty($fecha2)) {
  //     return $query->whereBetween(\Illuminate\Support\Facades\DB::raw("CAST(CREATED_TIME AS date)"), [$fecha1, $fecha2]);
  //   }
  // }
  public function toArray()
  {
    $array = parent::toArray();
    $newArray = array();
    foreach ($array as $name => $value) {
      $newArray[strtolower($name)] = $value;
    }
    return $newArray;
  }
}
```

21) Crear el archivo /app/Helpers/Helper.php y copiar las siguentes funciones : 
```php
<?php
use App\User;
use App\User_jwt;
if (!function_exists('jwtIsValid')) {
    function jwtIsValid($token, $request = [], $payload = [])
    {
        try {
            $user_agent = isset($request["user_agent_socket"]) ? $request["user_agent_socket"] : $_SERVER['HTTP_USER_AGENT'];
            $user_agent_ = isset($payload["u"]) && $payload["u"] != "" ? \Illuminate\Support\Facades\Crypt::decrypt($payload["u"]) : "";
            $ip = isset($request["ip_socket"]) ? $request["ip_socket"] : (isset($_SERVER['HTTP_X_FORWARDED_FOR']) ? $_SERVER['HTTP_X_FORWARDED_FOR'] : getIp());
            $ip_ = isset($payload["i"]) && $payload["i"] != "" ? \Illuminate\Support\Facades\Crypt::decrypt($payload["i"]) : "";
            $jwt = isset($payload["jti"]) && $payload["jti"] != "" ? \App\Models\User_jwt::where("JTI", $payload["jti"])->first() : "";
            if (isset($jwt) && isset($jwt->ID_PARENT)) {
                \App\Models\User_jwt::where('ID', $jwt->ID_PARENT)->update(['IS_VALID' => 0]);
                $invalidate = jwtInvalidate($token);
                return ['status' => 401, 'message' => "", 'code' => 13, "errorMessage" => "jwt 2 invalido(1)", "responseJSON" => ["message" => ""]];
            } else if ((string)$user_agent != (string)$user_agent_) {
                $invalidate = jwtInvalidate($token);
                return ['status' => 401, 'message' => "Unauthenticated.", 'code' => 7, 'i' => $invalidate, "errorMessage" => "dispositivo invalido (1)", "responseJSON" => ["message" => "Unauthenticated."]];
            } else if (isset($jwt) && (int)$jwt->IS_VALID == 0) {
                $invalidate = jwtInvalidate($token);
                return ['status' => 401, 'message' => "Unauthenticated.", 'code' => 9, 'i' => $invalidate, "errorMessage" => "token invalido (1)", "responseJSON" => ["message" => "Unauthenticated."]];
            } else if ((string)$ip != (string)$ip_) {
                // $invalidate = jwtInvalidate($token);
                $pin = isset($request["pin"]) ? $request["pin"] : "";
                $pin_ = isset($payload["p"]) && $payload["p"] != "" ? \Illuminate\Support\Facades\Crypt::decrypt($payload["p"]) : "";
                $exp_time = isset($request["exp_time"]) ? $request["exp_time"] : 3600;
                if($pin != "" && $pin_ != "" && (string)$pin == (string)$pin_){
                    session(['pin' => $pin]);
                    return ['status' => 401, 'message' => "Unauthenticated.", 'code' => 12, 'i' => "", "errorMessage" => "ip invalida (2)", "responseJSON" => ["message" => "Unauthenticated."], "refresh" => jwtLoginByUserId($payload["sub"], $exp_time, 1)];
                } else {
                    return ['status' => 401, 'message' => "Unauthenticated.", 'code' => 6, 'i' => "", "errorMessage" => "ip invalida (1)", "responseJSON" => ["message" => "Unauthenticated."]];
                }
            } else {
                return ['status' => 200, 'message' => "", 'code' => 10, "errorMessage" => "jwt valido (1)", "responseJSON" => ["message" => ""], "payload" => $payload];
            }
        } catch (Exception $e) {
            return ["status" => 401, "message" => $e, 'code' => 11, "errorMessage" => "error indefinido", "responseJSON" => ["message" => "Unauthenticated."]];
        }
    }
}
if (!function_exists('jwtSave')) {
    function jwtSave($token, $token2, $is_api = 1, $version)
    {
        // dd($token);
        // try {
            // $user = \JWTAuth::toUser();
            $payload = \JWTAuth::manager()->getJWTProvider()->decode($token);
            // $user = \App\User::where('DUI', $payload['dui'])->first();
            $user_jwt = new \App\Models\User_jwt;
            $user_jwt->JTI = $payload["jti"];
            $user_jwt->IP = $payload["i"];
            $user_jwt->DUI = $payload["dui"];
            // $user_jwt->ONI = $user["oni"];
            $user_jwt->USER_AGENT = $payload["u"];
            $user_jwt->EXP_TIME = $payload["exp"];
            $user_jwt->IS_VALID = 1;
            $user_jwt->IS_API = $is_api;
            $user_jwt->VERSION = $version;
            $user_jwt->CREATED_TIME = time();
            if ($user_jwt->save()) {
                // TOKEN 2
                $payload2 = \JWTAuth::manager()->getJWTProvider()->decode($token2);
                // $user = \App\User::where('DUI', $payload2['dui'])->first();
                $user_jwt2 = new \App\Models\User_jwt;
                $user_jwt2->JTI = $payload2["jti"];
                $user_jwt2->IP = $payload2["i"];
                $user_jwt2->DUI = $payload2["dui"];
                // $user_jwt2->ONI = $user["oni"];
                $user_jwt2->USER_AGENT = $payload2["u"];
                $user_jwt2->EXP_TIME = $payload2["exp"];
                $user_jwt2->IS_VALID = 1;
                $user_jwt2->IS_API = $is_api;
                $user_jwt2->VERSION = $version;
                $user_jwt2->ID_PARENT = $user_jwt->id;
                $user_jwt2->CREATED_TIME = time();
                $user_jwt2->save();
                return ["status" => 200, "data" => $user_jwt, "data2" => $user_jwt2, "message" => "", "jwt_id" => $user_jwt->id];
            }
        // } catch (Exception $e) {
        //     return ["status" => 400, "message" => $e];
        // }
    }
}
if (!function_exists('jwtLogin')) {
    function jwtLogin($credentials, $exp_time = 480, $version = "", $pin = "")
    {
        // $now = \Carbon\Carbon::now()->timestamp;
        // $exp = \Carbon\Carbon::now()->addSeconds($exp_time)->timestamp;
        // $sum = $exp - $now;
        // dd(["exp"=>$exp,"now"=>$now,"sum"=>$sum]);
        // try {
            // $myTTL = 30; //minutes
            \JWTAuth::factory()->setTTL($exp_time);
            session(['pin' => $pin]);
            $token = auth('api')->attempt($credentials);
            if ($token) {
                $token2 = auth('api')->attempt($credentials);
                $jwtSave = jwtSave($token, $token2, 1, $version);
                return ["status" => 200, "data" => $token, "data2" => $token2, "message" => "", "jwt_id" => $jwtSave["jwt_id"]];
            } else {
                return ["status" => 400, "data" => null, "message" => "Credenciales no validas"];
            }
        // } catch (Exception $e) {
        //     return ["status" => 400, "message" => $e];
        // }
    }
}
if (!function_exists('jwtPinWeb')) {
    function jwtPinWeb()
    {
        if (session('jwtPinWeb')) {
            return session('jwtPinWeb');
        } else {
            session(['jwtPinWeb' => \Illuminate\Support\Str::orderedUuid()]);
            return session('jwtPinWeb');
        }
    }
}
if (!function_exists('jwtLoginByUserId')) {
    function jwtLoginByUserId($user_id, $exp_time = 3600, $is_api = 0)
    {
        try {
            $user = User::where("ID", $user_id)->first();
            \JWTAuth::factory()->setTTL($exp_time);
            $token = auth('api')->login($user);
            if ($token) {
                $token2 = auth('api')->login($user);
                $jwtSave = jwtSave($token, $token2, $is_api, "");
                return ["status" => 200, "data" => $token, "data2" => $token2, "message" => "", "jwt_id" => $jwtSave["jwt_id"]];
            } else {
                return ["status" => 400, "data" => null, "message" => "No se pudo crear el token"];
            }
        } catch (Exception $e) {
            return ["status" => 400, "message" => $e];
        }
    }
}
if (!function_exists('jwtInvalidate')) {
    function jwtInvalidate($token)
    {
        try {
            // $payload = \JWTAuth::manager()->getJWTProvider()->decode($token);
            // \App\Models\User_jwt::where('JTI', $payload["jti"])->update(['IS_VALID' => 0]);
            // $data = auth('api')->invalidate(true);
            // \JWTAuth::manager()->invalidate(new \Tymon\JWTAuth\Token($token), $forceForever = false);
            return ["status" => 200, "data" => "", "message" => "token invalidado"];
        } catch (Exception $e) {
            return ["status" => 400, "data" => null, "message" => $e];
        }
    }
}
if (!function_exists('jwtLogout')) {
    function jwtLogout()
    {
        try {
            $data = auth('api')->logout(true);
            return ["status" => 200, "data" => $data, "message" => ""];
        } catch (Exception $e) {
            return ["status" => 400, "data" => null, "message" => $e];
        }
    }
}
if (!function_exists('jwtMe')) {
    function jwtMe()
    {
        try {
            $data = auth('api')->user();
            return ["status" => 200, "data" => $data, "message" => ""];
        } catch (Exception $e) {
            return ["status" => 400, "data" => null, "message" => $e];
        }
    }
}
if (!function_exists('jwtRefresh')) {
    function jwtRefresh()
    {
        try {
            $data = auth('api')->refresh(true);
            return ["status" => 200, "data" => $data, "message" => ""];
        } catch (Exception $e) {
            return ["status" => 400, "data" => null, "message" => $e];
        }
    }
}
if (!function_exists('jwtToken')) {
    function jwtToken($numero = 1)
    {
        if (auth()->user()) {
            if (session('jwt_token1')) {
                $jwt_token1 = session('jwt_token1');
                $isValid = jwtIsValid($jwt_token1,request(),\JWTAuth::manager()->getJWTProvider()->decode($jwt_token1));
                if ($isValid["status"] == 200) {
                    return ["jwt_token1" => $jwt_token1, "jwt_token2" => session('jwt_token2'), "isValid"=>$isValid];
                } else {
                    $userId = auth()->user()->id;
                    $jwt_res = jwtLoginByUserId($userId);
                    if ($jwt_res["status"] == 200) {
                        session(['jwt_token1' => $jwt_res["data"]]);
                        session(['jwt_token2' => $jwt_res["data2"]]);
                        return ["jwt_token1" => $jwt_res["data"], "jwt_token2" => $jwt_res["data2"]];
                    } else {
                        session(['jwt_token1' => ""]);
                        session(['jwt_token2' => ""]);
                        return ["jwt_token1" => "", "jwt_token2" => "", 'message' => "Credenciales Invalidas o usuario no existe",];
                    }
                }
            } else {
                $userId = auth()->user()->id;
                $jwt_res = jwtLoginByUserId($userId);
                if ($jwt_res["status"] == 200) {
                    session(['jwt_token1' => $jwt_res["data"]]);
                    session(['jwt_token2' => $jwt_res["data2"]]);
                    return ["jwt_token1" => $jwt_res["data"], "jwt_token2" => $jwt_res["data2"]];
                } else {
                    session(['jwt_token1' => ""]);
                    session(['jwt_token2' => ""]);
                    return ["jwt_token1" => "", "jwt_token2" => "", 'message' => "Credenciales Invalidas o usuario no existe",];
                }
            }
        } else {
            session(['jwt_token1' => ""]);
            session(['jwt_token2' => ""]);
            return ["jwt_token1" => "", "jwt_token2" => "", 'message' => "Ususario no logueado",];
        }
    }
}
if (!function_exists('getIp')) {
    function getIp()
    {
        foreach (array('HTTP_CLIENT_IP', 'HTTP_X_FORWARDED_FOR', 'HTTP_X_FORWARDED', 'HTTP_X_CLUSTER_CLIENT_IP', 'HTTP_FORWARDED_FOR', 'HTTP_FORWARDED', 'REMOTE_ADDR', 'X-Forwarded-For') as $key) {
            if (array_key_exists($key, $_SERVER) === true) {
                foreach (explode(',', $_SERVER[$key]) as $ip) {
                    $ip = trim($ip); // just to be safe
                    if (filter_var($ip, FILTER_VALIDATE_IP, FILTER_FLAG_NO_PRIV_RANGE | FILTER_FLAG_NO_RES_RANGE) !== false) {
                        return $ip;
                    }
                }
            }
        }
        return request()->ip(); // it will return server ip when no client ip found
    }
}
```

22) En el archivo composer.json agregar el helper el la siguente estructura : 
```json
 "autoload": {
    "psr-4": {
        "App\\": "app/"
    },
    "classmap": [
        "database/seeds",
        "database/factories"
    ],
    "files": [
        "app/Helpers/Helpers.php"
    ]
},
```

23) Ejecutar el comando en terminal para que reconosca el nuevo helper : 
```console
composer dump-autoload
```

24) Ejecutar siguente comando :
```console
php artisan make:middleware JwtMiddleware
```

25) Copiar y pegar el siguente comando en el archivo /app/Http/Middleware/JwtMiddleware.php :
```php
<?php
namespace App\Http\Middleware;
use App\Providers\RouteServiceProvider;
use App\User;
use App\Models\Mtto\Enlace;
use Illuminate\Support\Collection;
use Tymon\JWTAuth;
use Closure;
use Exception;
use Tymon\JWTAuth\Http\Middleware\BaseMiddleware;
class JwtMiddleware extends BaseMiddleware
{
  /**
   * Handle an incoming request.
   *
   * @param  \Illuminate\Http\Request  $request
   * @param  \Closure  $next
   * @return mixed
   */
  public function handle($request, Closure $next)
  {
    if (isset($request->debug_secret) && $request->debug_secret == env('APP_KEY') . env('JWT_SECRET')) { config(['app.debug' => true]); }
    try {
      $user = \JWTAuth::parseToken()->authenticate();
      if ($user) {
        $token = \JWTAuth::getToken()->get();
        $payload = \JWTAuth::manager()->getJWTProvider()->decode($token);
        // $macAddr = exec('getmac');
        // return response()->json($macAddr); 
        $isValid = jwtIsValid($token, $request, $payload);
        // return response()->json($request->all());
        if ($isValid["status"] == 200) {
          $isValid["payload"]["exp_time"] = $isValid["payload"]["exp"] - $isValid["payload"]["iat"];
          $isValid["payload"]["token"] = $token;
          $isValid["payload"]["lifetime"] = $isValid["payload"]["exp"] - time();
          $request->merge(['jwt' => $isValid]);
          return $next($request);
        } else {
          return response()->json($isValid); 
        }
      } else {
        return response()->json(['status' => 401, 'message' => "Unauthenticated.", 'code' => 5, "errorMessage" => "Usuario no encontrado", "responseJSON" => ["message" => "Unauthenticated."]]);
      }
    } catch (Exception $e) {
      if ($e instanceof JWTAuth\Exceptions\TokenInvalidException) {
        return response()->json(['status' => 401, 'message' => "Unauthenticated.", 'code' => 4, "errorMessage" => $e->getMessage(), "responseJSON" => ["message" => "Unauthenticated."]]);
      } else if ($e instanceof JWTAuth\Exceptions\TokenExpiredException) {
        return response()->json(['status' => 401, 'message' => "Unauthenticated.", 'code' => 3, "errorMessage" => $e->getMessage(), "responseJSON" => ["message" => "Unauthenticated."]]);
      } else if ($e instanceof JWTAuth\Exceptions\JWTException) {
        return response()->json(['status' => 401, 'message' => "Unauthenticated.", 'code' => 2, "errorMessage" => $e->getMessage(), "responseJSON" => ["message" => "Unauthenticated."]]);
      } else {
        return response()->json(['status' => 401, 'message' => "Unauthenticated.", 'code' => 1, "errorMessage" => $e->getMessage(), "responseJSON" => ["message" => "Unauthenticated."]]);
      }
    }
  }
}
```

26) Agregar el siguente codigo al archivo /app/Http/kernel.php :
```php
protected $routeMiddleware = [
    /* ... */
    'jwt' => \App\Http\Middleware\JwtMiddleware::class,
];
```

27) Crear proyecto de angular en la terminal llamado angubase:
```console
ng serve --port=85
```

28) Ir al navegador :
```console
localhost:85
```

29) Generar archivos de tipo "service" que son el nucleo de la logica :
```console
ng generate service services/auth && ng generate service services/api && ng generate service services/core ng generate service services/helper
```

30) Generar archivos de tipo "component" y "module" para la generacion de pantallas (--flat sirve para generar los archivos en el mismo directorio):
```console
ng g m pages/login --routing && ng g c pages/login/login --module=pages/login/login.module.ts --flat
```
```console
ng g m pages/loader --routing && ng g c pages/loader/loader --module=pages/loader/loader.module.ts --flat
```
```console
ng g m pages/home --routing && ng g c pages/home/home --module=pages/home/home.module.ts --flat
```

31) Editar el archivo /src/app/app-routing.module.ts con las siguentes rutas :
```js
import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';
import { AuthGuard } from './helpers/auth.guard';
const routes: Routes = [
  { path: '', redirectTo: 'loader', pathMatch: 'full' },
  { path: 'loader', loadChildren: () => import('./pages/loader/loader.module').then(m => m.LoaderModule) },
  { path: 'login', loadChildren: () => import('./pages/login/login.module').then(m => m.LoginModule) },
  { path: 'home', loadChildren: () => import('./pages/home/home.module').then(m => m.HomeModule), canActivate: [AuthGuard], data: { preload: true } },
  // otherwise redirect to loader
  { path: '**', redirectTo: 'loader', pathMatch: 'full' },
];
@NgModule({
  imports: [RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })],
  exports: [RouterModule]
})
export class AppRoutingModule { }
```

32) Editar el archivo /src/app/helpers/auth.guard.ts con las siguentes rutas :
```js


```

